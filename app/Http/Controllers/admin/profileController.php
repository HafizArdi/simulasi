<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Repositories\UserRepository;
use Hash;
use Auth;

class profileController extends Controller{

    public function index(){        
        $profile = User::find(Auth::user()->id);
		return view('admin.profile.index',compact('profile'));
    }

    public function update(Request $request, $id){
        $detail = User::find($id);
        $validate = [
            'name' => 'required|max:191',
            'username' => 'required|max:191',
            'email' => 'required|max:191',
            'password' => 'required|max:191',
          
        ];
        
        $this->validate(request(), $validate);
        $detail->name = $request->get('name');
        $detail->email = $request->get('email');
        $detail->username = $request->get('username');
        $detail->password = Hash::make($request->get('password'));
        $detail->save();
        \Session::flash('notif-success', 'Update data berhasil');
        return redirect()->route('profile.index')->with('success', 'Successfully created.');
    }
}
