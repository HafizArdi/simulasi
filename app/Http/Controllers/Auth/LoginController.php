<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class LoginController extends Controller{

    use AuthenticatesUsers;

  
    protected $redirectTo = '/';

    public function __construct(){
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
    }

    public function showAdminLoginForm(){
        return view('auth.login', ['url' => 'admin']);
    
    }

    public function login(Request $request){

        $this->validate($request, [
            'username'   => 'required',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(
            [
                'username' => $request->username,
                'password' => $request->password,
                
            ], $request->get('remember'))) {

                if(Auth::guard('admin')->User()->hasRole('admin')){
                    return redirect()->intended(route('admin.home'));
                }elseif(Auth::guard('admin')->User()->hasRole('guru')){
                    return redirect()->intended(route('admin.home'));
                }elseif(Auth::guard('admin')->User()->hasRole('siswa')){
                    return redirect()->intended(route('admin.home'));
                }
            }

        \Session::flash('notif-error', 'Failed to login.');
        return back()->withInput($request->only('username', 'remember'));
    }
}
