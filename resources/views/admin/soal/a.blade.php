<form role="form" action="{{ route('soal.pertanyaanUpdate', $pertanyaan->idMapel) }}" method="POST">
						{{ csrf_field() }}
							<div id="looping">
								<div class="row">
									<div class="col-md-12">
										<label class="bmd-label-floating">Soal No 1</label>
										<div class="form-group">
											<textarea rows="3" type="text" class="form-control" name="soal[]" value="">{{old('soal1', $pertanyaan->pertanyaan1)}}</textarea>
											@if($errors->has('soal1'))
												<span class="help-block text-danger">
												<strong>{{$errors->first('soal1') }}</strong>
												</span>
											@endif
										</div>
										<div class="form-group">
											<label class="bmd-label-floating">Pilihan A</label>
											<input type="text" class="form-control" name="pilgan1[]" value="">
											@if($errors->has('pilgan1'))
												<span class="help-block text-danger">
												<strong>{{$errors->first('pilgan1') }}</strong>
												</span>
											@endif
										</div>
										<div class="form-group">
											<label class="bmd-label-floating">Pilihan B</label>
											<input type="text" class="form-control" name="pilgan2[]" value="">
											@if($errors->has('soal1'))
												<span class="help-block text-danger">
												<strong>{{$errors->first('soal1') }}</strong>
												</span>
											@endif
										</div>
										<div class="form-group">
											<label class="bmd-label-floating">Pilihan C</label>
											<input type="text" class="form-control" name="pilgan3[]" value="">
											@if($errors->has('soal1'))
												<span class="help-block text-danger">
												<strong>{{$errors->first('soal1') }}</strong>
												</span>
											@endif
										</div>
										<div class="form-group">
											<label class="bmd-label-floating">Pilihan D</label>
											<input type="text" class="form-control" name="pilgan4[]" value="">
											@if($errors->has('soal1'))
												<span class="help-block text-danger">
												<strong>{{$errors->first('soal1') }}</strong>
												</span>
											@endif
										</div>
										<!-- <div class="form-group">
											<label class="bmd-label-floating">Kunci Jawaban:</label>
											<input type="text" class="form-control" name="kunci[]" value="">
											@if($errors->has('soal1'))
												<span class="help-block text-danger">
												<strong>{{$errors->first('soal1') }}</strong>
												</span>
											@endif
										</div> -->
									</div>
									<input type="hidden" name="keterangan[]" value="pilgan1"/>
								</div><br><br><br><hr>
							</div>
							<button type="submit" class="btn btn-primary pull-right">Update</button>
							<div class="clearfix"></div>
						</form>
@section('extrascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script>
	$(document).ready(function(){
		var $no = 2;

		$("#add").click(function(){
			$("#looping").append('<div class="row"><div class="col-md-12"><label class="bmd-label-floating">Soal No'+$no+'</label><div class="form-group"><textarea rows="3" type="text" class="form-control" name="soal[]" value="">{{old("soal1", $pertanyaan->pertanyaan1)}}</textarea>    </div></div></div>');
			$no++
		});
	});
</script>
@endsection