@extends('admin.layouts.app')
@section('content')
@section('profile', 'active')
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-header-primary">
						<h4 class="card-title">Edit Profile</h4>
					</div>
					<div class="card-body">
						<form role="form" action="{{route('profile.update', $profile->id)}}" method="POST">
						{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="bmd-label-floating">Name</label>
										<input type="text" class="form-control" name="name" value="{{old('name', $profile->name)}}">
										@if($errors->has('name'))
											<span class="help-block text-danger">
											<strong>{{$errors->first('name') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="bmd-label-floating">NPSN</label>
										<input type="number" class="form-control" max="10000000000" min="0" name="username" value="{{old('username', $profile->username) }}">
										@if($errors->has('username'))
											<span class="help-block text-danger">
											<strong>{{$errors->first('username') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="bmd-label-floating">Email</label>
										<input type="email" class="form-control" name="email" value="{{old('email', $profile->email) }}">
										@if($errors->has('email'))
											<span class="help-block text-danger">
											<strong>{{$errors->first('email') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="bmd-label-floating">Password</label>
										<input type="password" class="form-control" name="password">
										@if($errors->has('password'))
											<span class="help-block text-danger">
											<strong>{{$errors->first('password') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary pull-right">Update</button>
							<div class="clearfix"></div>
						</form>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection